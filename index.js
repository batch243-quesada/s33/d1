// console.log('Hello, World');

// JavaScript Synchronous vs Asynchronous
// JS is by default synchronous which means that only one statement can be executed at a time

// code blocking

// Asynchronous means that we can proceed to execute other statements, while time consuming codes are running in the background

// The fetch API allows you to asynchrounously request for a resource (data)
// syntax
// fetch('URL')
// .then((response) => {})


//// CHECK THE STATUS OF THE REQUEST
// By using the .then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status));

// The fetch method will return a promise that resolves to a response object
// The .then method captures the response object and returns another promise which will eventually be resolved or rejected

//// RETRIEEV CONTENTS/DATA FROM THE RESPONSE OBJECT
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())

// using multiple .then method creates a "promise chain:"
.then(json => console.log(json))

//// CREATE A FUNCTION THAT WILL DEMONSTRATE USING async and await KEYWORDS

// The async and await keywords is another approach that can be used to achieve an asynchronous code
// Used in functions to indicate which portions of code should be waited for
// Creates a "synchronous" function

async function fetchData() {

	// waits for the fetch method to complete then stores the value in the result variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result);

	// result returned by fetch is actually a "promise"
	console.log(result);

	// The returned response is an objeect
	console.log(typeof result);

	// We can not access the content of the response directly by accessin its body property
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}

fetchData();

//// RETRIEVE A SPECIFIC POST
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then(response => response.json())
.then(json => console.log(json))

//// CREATE POST
// syntax
// fetch('URL', options)
// .then((response) => {})
// .then((response) => {})

fetch('https://jsonplaceholder.typicode.com/posts', {
	
	// Sets the method of the request object to POST
	method : 'POST',

	//Speciefied that the content will be ina JSON structure
	headers: {
		'Content-Type' : 'application/json'
	},

	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello World',
		userID: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));


//// UPDATE A POST USING PUT METHOD
fetch('https://jsonplaceholder.typicode.com/posts/1', {

	// Put is used to update a single property
	method : 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello again!',
		userID: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


//// UPDATE A POST USING PATCH METHID
// Patch is used to update the whole object
// Put is used to update a single property

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method : 'PATCH',
	headers : {
		'Content-Type' : 'application/json'
	},
	body : JSON.stringify({
		title: 'Corrected post'
	})
})
.then(response => response.json())
.then(json => console.log(json));


//// DELETE A POST
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method : 'DELETE'
}) 